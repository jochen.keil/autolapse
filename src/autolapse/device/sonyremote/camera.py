from sonyremoteapi import CameraRemoteAPI, CameraStatus

from autolapse.interface import Camera, Control, Preview, Setting

from .preview import SonyRemotePreview
from .controls import SonyIsoControl, SonyFNumberControl, SonyShutterSpeedControl

class SonyRemote(Camera):
  def __init__(self, address=None):
    super().__init__()

    self.__address = address
    self.__camera = CameraRemoteAPI()
    self.__preview = None

    self.__methods = [ 'getAvailableFNumber'
                     , 'getAvailableShutterSpeed'
                     , 'getAvailableIsoSpeedRate'
                     , 'getFNumber'
                     , 'setFNumber'
                     , 'getShutterSpeed'
                     , 'setShutterSpeed'
                     , 'getIsoSpeedRate'
                     , 'setIsoSpeedRate'
                     , 'startLiveview'
                     , 'actTakePicture'
                     ]

  def remote_api(self):
    return self.__camera

  def connect(self):
    self.__camera.connect(address=self.__address)
    if self.__camera.is_available('startRecMode'):
      self.__camera.startRecMode()
    self.__camera.wait_for_methods(self.__methods)

    self.register(Setting.ISO, SonyIsoControl(self.__camera))
    self.register(Setting.FNumber, SonyFNumberControl(self.__camera))
    self.register(Setting.ShutterSpeed, SonyShutterSpeedControl(self.__camera))

  def disconnect(self):
    try:
      self.__preview.stop()
    except:
      pass
    self.__camera.disconnect()

  def preview(self) -> Preview:
    if self.__preview is None:
      self.__preview = SonyRemotePreview(self.__camera)
      self.__preview.start()
    return self.__preview

  def capture(self):
    self.__camera.wait_for_status(CameraStatus.IDLE)
    self.__camera.actTakePicture()

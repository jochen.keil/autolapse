from io import BytesIO
from PIL import Image

from autolapse.utils import brightness
from autolapse.interface import Preview

from sonyremoteapi import CameraRemoteAPI, Liveview

class SonyRemotePreview(Preview):
  def __init__(self, camera: CameraRemoteAPI):
    self.__liveview = Liveview(camera)

  def start(self):
    return self.__liveview.start()

  def stop(self):
    return self.__liveview.stop()

  def image(self):
    return Image.open(BytesIO(self.__liveview.image()))

  def brightness(self):
    return brightness(self.image())

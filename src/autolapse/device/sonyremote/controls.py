from abc import ABCMeta, abstractmethod
from fractions import Fraction
from typing import TypeVar, Tuple, Sequence

from autolapse.interface import Control, Sensitivity

from sonyremoteapi import CameraRemoteAPI

T = TypeVar('T')

class SonyControl(Control[T], metaclass=ABCMeta):
  @abstractmethod
  def get_camera_value(self) -> str:
    raise NotImplementedError

  @abstractmethod
  def set_camera_value(self, v: Sequence[str]) -> bool:
    raise NotImplementedError

  @abstractmethod
  def get_available_range(self) -> Tuple[str, Sequence[str]]:
    raise NotImplementedError

  @abstractmethod
  def get_next_index(self, sensitivity: Sensitivity, index: int) -> int:
    raise NotImplementedError

  @abstractmethod
  def convert(self, value: str) -> T:
    raise NotImplementedError

  def get(self) -> T:
    return self.convert(self.get_camera_value())

  def set(self, value: T) -> bool:
    self.set_camera_value(str(value))

  def range(self) -> Sequence[T]:
    return list(filter(lambda v: v is not None,
                  map(lambda v: self.convert(v),
                    self.get_available_range()[1])))

  def change(self, sensitivity: Sensitivity) -> T:
    result = self.get_available_range()
    values = result[1]
    current = result[0]
    index = self.get_next_index(sensitivity, values.index(current))

    if index >= 0 and index < len(values):
      v = self.convert(values[index])
      self.set(v)
      return v

    return self.convert(current)

class SonyIsoControl(SonyControl[int]):
  def __init__(self, camera: CameraRemoteAPI):
    self.__camera = camera

  def get_camera_value(self) -> str:
    return self.__camera.getIsoSpeedRate()

  def set_camera_value(self, value: Sequence[str]) -> bool:
    return self.__camera.setIsoSpeedRate(value)

  def get_available_range(self) -> Tuple[str, Sequence[str]]:
    return self.__camera.getAvailableIsoSpeedRate()

  def get_next_index(self, sensitivity: Sensitivity, index: int) -> int:
    if sensitivity == Sensitivity.Lower:
      return index - 1
    else:
      return index + 1

  def convert(self, value: str) -> int:
    try:
      return int(value)
    except:
      return None

class SonyFNumberControl(SonyControl[float]):
  def __init__(self, camera: CameraRemoteAPI):
    self.__camera = camera

  def get_camera_value(self) -> str:
    return self.__camera.getFNumber()

  def set_camera_value(self, value: Sequence[str]) -> bool:
    return self.__camera.setFNumber(value)

  def get_available_range(self) -> Tuple[str, Sequence[str]]:
    return self.__camera.getAvailableFNumber()

  def get_next_index(self, sensitivity: Sensitivity, index: int) -> int:
    if sensitivity == Sensitivity.Narrower:
      return index + 1
    else:
      return index - 1

  def convert(self, value: str) -> float:
    try:
      return float(value)
    except:
      return None

class SonyShutterSpeedControl(SonyControl[Fraction]):
  def __init__(self, camera: CameraRemoteAPI):
    self.__camera = camera

  def get_camera_value(self) -> str:
    return self.__camera.getShutterSpeed()

  def set_camera_value(self, value: Sequence[str]) -> bool:
    return self.__camera.setShutterSpeed(value)

  def get_available_range(self) -> Tuple[str, Sequence[str]]:
    return self.__camera.getAvailableShutterSpeed()

  def get_next_index(self, sensitivity: Sensitivity, index: int) -> int:
    if sensitivity == Sensitivity.Faster:
      return index + 1
    else:
      return index - 1

  def convert(self, value: str) -> Fraction:
    try:
      return Fraction(value)
    except:
      return None

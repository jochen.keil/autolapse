from time import sleep
from autolapse.interface import Trigger

class Timer(Trigger):
  def __init__(self, count=None, delay=1):
    self.__count = count
    self.__delay = delay

  def wait(self):
    sleep(self.__delay)
    if self.__count:
      self.__count = self.__count - 1
      return self.__count > 0
    else:
      return True

from abc import ABCMeta, abstractmethod
from typing import Generic, TypeVar, Sequence

from .sensitivity import Sensitivity

T = TypeVar('T')

class Control(Generic[T], metaclass=ABCMeta):
  @abstractmethod
  def get(self) -> T:
    raise NotImplementedError

  @abstractmethod
  def set(self, value: T) -> bool:
    raise NotImplementedError

  @abstractmethod
  def range(self) -> Sequence[T]:
    raise NotImplementedError

  @abstractmethod
  def change(self, sensitivity: Sensitivity) -> T:
    raise NotImplementedError

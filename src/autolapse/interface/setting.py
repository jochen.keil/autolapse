from enum import Enum

class Setting(Enum):
  ISO = 0
  FNumber = 1
  ShutterSpeed = 2

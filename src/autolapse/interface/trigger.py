from abc import ABCMeta, abstractmethod

class Trigger(metaclass=ABCMeta):
  @abstractmethod
  def wait(self) -> bool:
    raise NotImplementedError

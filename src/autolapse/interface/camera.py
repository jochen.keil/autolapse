from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Sequence

from .control import Control
from .preview import Preview
from .setting import Setting

class Camera(metaclass=ABCMeta):
  def __init__(self) -> None:
    self.__controls: Dict[Setting, Control[Any]] = dict()

  @abstractmethod
  def connect(self) -> bool:
    raise NotImplementedError

  @abstractmethod
  def disconnect(self) -> bool:
    raise NotImplementedError

  @abstractmethod
  def capture(self) -> bool:
    raise NotImplementedError

  @abstractmethod
  def preview(self) -> Preview:
    raise NotImplementedError

  def control(self, setting: Setting) -> Control[Any]:
    return self.__controls[setting]

  def register(self, setting: Setting, control: Control[Any]) -> None:
    self.__controls[setting] = control

  def calibrate(self) -> float:
    '''Return a float value which reflects how brightness changes for one stop
    :returns: Brightness change from 0.0 to 1.0 (0% to 100%) for 1 EV
    :rtype: float
    '''

    initial_brightness = self.preview().brightness()
    iso_range: Sequence[Any] = self.control(Setting.ISO).range()
    current_iso: Any = self.control(Setting.ISO).get()
    index = iso_range.index(current_iso)

    if index < len(iso_range) - 3:
      next_stop = index + 3
    else:
      next_stop = index - 3

    self.control(Setting.ISO).set(iso_range[next_stop])
    current_brightness = self.preview().brightness()
    self.control(Setting.ISO).set(iso_range[index])

    return float(abs(initial_brightness - current_brightness) / current_brightness)

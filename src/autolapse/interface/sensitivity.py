from enum import Enum

class Sensitivity(Enum):
  # less sensitivity
  Lower = Faster = Narrower = 0
  # more sensitivity
  Higher = Slower = Wider = 1

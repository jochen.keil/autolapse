from abc import ABCMeta, abstractmethod

class Preview(metaclass=ABCMeta):
  @abstractmethod
  def brightness(self) -> int:
    raise NotImplementedError

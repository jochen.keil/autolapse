from dataclasses import dataclass
from .mode import Mode

@dataclass
class MinMax:
  min: str
  max: str

@dataclass
class Config:
  mode: Mode
  iso: MinMax
  fnumber: MinMax
  shutterspeed: MinMax
  brightness_average_frames: int = 3

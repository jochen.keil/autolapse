from threading import Event, Thread
from statistics import mean
from collections import deque

from autolapse.interface import Camera, Trigger, Setting, Sensitivity

from .mode import Mode
from .config import Config

class Controller(Thread):
  '''
  :param brightness_ma_window_size: Moving average window size for brightness
  '''
  def __init__(self, config: Config, camera: Camera, trigger: Trigger, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__config = config
    self.__camera = camera
    self.__trigger = trigger
    self.__cancel = Event()

  def cancel(self):
    self.__cancel.set()

  def connect(self):
    self.__camera.connect()

  def disconnect(self):
    self.__camera.disconnect()

  def run(self):
    # print('timelapsecontroller.run() begin')
    currentCount = 0

    mode = self.__config.mode

    iso_control = self.__camera.control(Setting.ISO)
    fnumber_control = self.__camera.control(Setting.FNumber)
    shutterspeed_control = self.__camera.control(Setting.ShutterSpeed)

    iso_min = iso_control.convert(self.__config.iso.min)
    iso_max = iso_control.convert(self.__config.iso.max)
    fnumber_min = fnumber_control.convert(self.__config.fnumber.min)
    fnumber_max = fnumber_control.convert(self.__config.fnumber.max)
    shutterspeed_min = shutterspeed_control.convert(self.__config.shutterspeed.min)
    shutterspeed_max = shutterspeed_control.convert(self.__config.shutterspeed.max)

    one_stop = self.__camera.calibrate()
    third_stop = one_stop / 3
    print('one_stop', one_stop)
    print('third_stop', third_stop)

    # print('timelapsecontroller.run() get brightness')
    brightness_ma = deque(maxlen=self.__config.brightness_average_frames)

    print('timelapsecontroller.run() while next')
    while not self.__cancel.is_set() and self.__trigger.wait():

      self.__camera.capture()

      currentCount = currentCount + 1



      delta = 0
      brightness_changed = False
      current_brightness = self.__camera.preview().brightness()

      if len(brightness_ma) == brightness_ma.maxlen:
        delta = mean(brightness_ma) - current_brightness
        brightness_changed = abs(delta) >= third_stop

      brightness_ma.append(current_brightness)

      if (mode == Mode.Sunset):
        if brightness_changed and delta > 0:
          if fnumber_control.get() > fnumber_min:
            v = fnumber_control.change(Sensitivity.Wider)
            print("Setting aperture to {}".format(v))
          elif shutterspeed_control.get() < shutterspeed_max:
            v = shutterspeed_control.change(Sensitivity.Slower)
            print("Setting shutterspeed to {}".format(v))
          elif iso_control.get() < iso_max:
            v = iso_control.change(Sensitivity.Higher)
            print("Setting ISO to {}".format(v))

      if (mode == Mode.Sunrise):
        if brightness_changed and delta < 0:
          if iso_control.get() > iso_min:
            v = iso_control.change(Sensitivity.Lower)
            print("Setting ISO to {}".format(v))
          elif shutterspeed_control.get() > shutterspeed_min:
            v = shutterspeed_control.change(Sensitivity.Faster)
            print("Setting shutterspeed to {}".format(v))
          elif fnumber_control.get() < fnumber_max:
            v = fnumber_control.change(Setting.Narrower)
            print("Setting aperture to {}".format(v))

      # if brightness_changed:
      #   previousBrightness = current_brightness

      # except Exception as e:
      #   print(e)

from time import sleep
from argparse import ArgumentParser

from autolapse import Config, Controller, Mode, MinMax, Setting
# from autolapse.controller import TimeLapseController, TimeLapseMode

from autolapse.device.sonyremote import SonyRemote, \
                                        SonyIsoControl, \
                                        SonyFNumberControl, \
                                        SonyShutterSpeedControl

from autolapse.device.trigger import Timer

def get_args():
  parser = ArgumentParser()
  parser.add_argument('--address', default=None)
  parser.add_argument('--mode', default='sunset')
  parser.add_argument('--iso-min', default='100')
  parser.add_argument('--iso-max', default='3200')
  parser.add_argument('--fnumber-min', default='2.8')
  parser.add_argument('--fnumber-max', default='16.0')
  parser.add_argument('--shutterspeed-min', default='1/1000')
  parser.add_argument('--shutterspeed-max', default='1/1')
  return parser.parse_args()

def get_mode(args):
  if args.mode == 'sunset':
    return Mode.Sunset
  else:
    return Mode.Sunrise

def get_iso(args):
  return MinMax(args.iso_min, args.iso_max)

def get_fnumber(args):
  return MinMax(args.fnumber_min, args.fnumber_max)

def get_shutterspeed(args):
  return MinMax(args.shutterspeed_min, args.shutterspeed_max)

def get_config(args):
  mode = get_mode(args)
  iso_minmax = get_iso(args)
  fnumber_minmax = get_fnumber(args)
  shutterspeed_minmax = get_shutterspeed(args)
  return Config(mode, iso_minmax, fnumber_minmax, shutterspeed_minmax)

def main():
  args = get_args()
  config = get_config(args)

  sonyremote = SonyRemote(address=args.address)
  iso_control = SonyIsoControl(sonyremote.remote_api())
  fnumber_control = SonyFNumberControl(sonyremote.remote_api())
  shutterspeed_control = SonyShutterSpeedControl(sonyremote.remote_api())

  sonyremote.register(Setting.ISO, iso_control)
  sonyremote.register(Setting.FNumber, fnumber_control)
  sonyremote.register(Setting.ShutterSpeed, shutterspeed_control)

  trigger = Timer(delay=1, count=30)

  controller = Controller(config, sonyremote, trigger)

  controller.connect()

  sonyremote.control(Setting.ISO).set(config.iso.min)
  sonyremote.control(Setting.FNumber).set(config.fnumber.max)
  sonyremote.control(Setting.ShutterSpeed).set(config.shutterspeed.min)

  # sleep(2)
  # input("Press Enter to continue")

  def handle_exception(e):
    print(e)
    controller.disconnect()

  try:
    print('camera set up')
    # sleep(2)
    controller.run()
    print('run done')
    controller.disconnect()
  except Exception as e:
    handle_exception(e)
  except BaseException as e:
    handle_exception(e)

if __name__ == '__main__':
  main()

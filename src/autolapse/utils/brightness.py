import math
from PIL import Image, ImageStat

# https://stackoverflow.com/a/3498247/1431609
def brightness(image: Image):
  stat = ImageStat.Stat(image)
  r, g, b = stat.mean
  return math.sqrt(0.241*(r**2) + 0.691*(g**2) + 0.068*(b**2))

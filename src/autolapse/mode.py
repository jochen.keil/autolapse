from enum import Enum

class Mode(Enum):
  Sunset = 0
  Sunrise = 1
